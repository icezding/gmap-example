package com.example.gmaplauncherexample;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.LatLngBounds.Builder;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MainActivity extends FragmentActivity {

	public final String LOG_TAG = "GMAP-POLYLINE";
	
	private GoogleMap mMap;
	
	private Timer adjustZoomTimer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
//		drawMarkerExample();
		
		drawPolyLineExample();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
	public void drawMarkerExample() {
        LatLng hongkong = new LatLng(22.25, 114.1667);

        drawMarker(hongkong);		
	}
	
	public void drawMarker(LatLng latLng) {
		GoogleMap map = ((SupportMapFragment)  getSupportFragmentManager().findFragmentById(R.id.map))
	               .getMap();
		
        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));

        map.addMarker(new MarkerOptions()
                .title("This is title")
                .snippet("this is snippet.")
                .position(latLng));		
	}
	
	public void drawPolyLineExample () {
		/*
		 * obtain from google map API
		 * http://maps.googleapis.com/maps/api/directions/json?origin=%E8%A7%80%E5%A1%98&destination=%E9%A6%AC%E9%9E%8D%E5%B1%B1&sensor=true&mode=driving&language=zh-TW&unit=metric&region=hk
		 */
		String encodedPolylineSample = "sucgCmndxTHLRb@DFFNTj@HVBLJt@@T?LAv@A^CNCPOn@IZKVEH]h@[^A@?@C?UF_@\\_E~ByRlNgB|Ae]t\\ILMPKPGJYh@Ud@Y`AW`AADEd@C`@?JA~@AvAC`AALEb@Mz@Op@CLK^Qf@Sb@Wd@ABW^]b@WXe@b@WTsCxBQL}AnAOL_@\\cAbAo@n@o@n@yAbBq@t@_@^[VWPSJYJw@RMBc@DyAHQ@_DPiCNS?K?K?K?O?Q?c@Ee@Ey@Kc@E[Gg@Cq@OMEa@KUIYKkAi@i@Uo@Su@IUCa@Aa@Bu@Fu@JaANg@Bu@?I?yACyACO?y@AGAI?I?I?o@AwACyCCsD?i@D{@JcAPoC~@oAb@s@TsAb@C@SFOFi@R_@Ni@RSFUHc@L[HE?SD_@Fg@Ha@Dc@DI?_@@c@@[?k@AwAQ{@OkAWyAa@_Ac@cDgBg@YgKeFoIcDsIqBgJqBod@eHcPkCsKaB_Gu@ycAqQ_CQwD@wDd@m@NeCt@i@^{@l@C@oAdAgBlByBxC{AhAmBt@iAX_@HyARUAk@BW@Y@Q@aCBw@C}AGoAMiAKA?[Ck@GUASAGAm@?s@K_@E]EsDc@kAMoEc@iBOmDGgAFg@FM@MBOBaARc@NwEvBsAn@iBp@mDr@iAH_ADiABq@Ae@Co@Cc@EuAQcDs@mCs@{CcAmDkA{BwAyA}@w@g@qBsA}BmB_CsBoBqBqBwBm@s@o@w@o@}@";
	
		// decode the polyline into a series of points
		ArrayList<LatLng> polylinePoints = decodePolyline(encodedPolylineSample);

		mMap = ((SupportMapFragment)  getSupportFragmentManager().findFragmentById(R.id.map))
	               .getMap();
		
		// draw them on the map and plot line between each of them
		mMap.addPolyline(new PolylineOptions()
				.addAll(polylinePoints)
				.width(5)
				.color(Color.BLUE));

		/*
		 *  latLngBounds - a square boundary on map
		 *  latLngBounds.Builder - builder to create latLngBounds
		 *  
		 *  create a LatLngBounds covering all the points in the polyline above
		 */
		Builder bounds = new LatLngBounds.Builder();
		for(LatLng point : polylinePoints) {
			bounds = bounds.include(point);
		}

		final LatLngBounds routeBounds = bounds.build();	
			
		/*
		 * move viewport of map to the polyline
		 * center the viewport to the center of LatLngBounds
		 * set the zoom level max - 5 as default, properly to "close" and cannot see all points
		 */
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(routeBounds.getCenter(), mMap.getMaxZoomLevel() - 5));
		
		/*
		 * set a timer to adjust the zooming level of viewport
		 * delay the operation of finding the appropriate zoom level by giving the Maps the time to adjust the zooming before further adjustment 
		 */
		TimerTask task = new TimerTask() {

            public void run() {
                     runOnUiThread(new Runnable() {
                                            
                                            @Override
                                            public void run() {
                                            	setMapZoomLevelToFitBound(routeBounds);
                                            }
                                    });
                     
            }
        };

        adjustZoomTimer = new Timer();
        adjustZoomTimer.schedule(task, 1000);// schedule Map display in 1 seconds
	}
	
	protected void setMapZoomLevelToFitBound(LatLngBounds targetBound) {
		
		boolean foundAllPoints = false;
		float currentZoomLevel = mMap.getCameraPosition().zoom;
		float mapZoonMinimum = mMap.getMinZoomLevel();
		ArrayList<LatLng> foundPoints = new ArrayList<LatLng>();
		ArrayList<LatLng> pointsToBeSearch = new ArrayList<LatLng>();
		pointsToBeSearch.add(targetBound.northeast);
		pointsToBeSearch.add(targetBound.southwest);
		
		Log.d(LOG_TAG, "northeast " + targetBound.northeast.latitude + ", " + targetBound.northeast.longitude);
		Log.d(LOG_TAG, "southwest " + targetBound.southwest.latitude + ", " + targetBound.southwest.longitude);
		
		while (!foundAllPoints && currentZoomLevel >= mapZoonMinimum) {
			
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(targetBound.getCenter(), currentZoomLevel));
			
			LatLngBounds viewportBounds = mMap.getProjection().getVisibleRegion().latLngBounds;
//			Log.d(LOG_TAG, "currentZoomLevel " + currentZoomLevel);
//			Log.d(LOG_TAG, "viewportBounds northeast " + viewportBounds.northeast.latitude + ", " + viewportBounds.northeast.longitude);
//			Log.d(LOG_TAG, "viewportBounds southwest " + viewportBounds.southwest.latitude + ", " + viewportBounds.southwest.longitude);
//			
			for (LatLng point : pointsToBeSearch) {
				
				if (foundPoints.contains(point)) {
					continue;
				}
				
				if (viewportBounds.contains(point)) {
					foundPoints.add(point);
				}
			}
			
			if (foundPoints.size() == pointsToBeSearch.size()) {
				adjustZoomTimer.cancel();
				break;
			}
			
			currentZoomLevel--;
		}
	}
	
	protected ArrayList<LatLng> decodePolyline(String encodedPolyline) {
		// Log.i("Location", "String received: "+encoded);
		ArrayList<LatLng> poly = new ArrayList<LatLng>();
		int index = 0, len = encodedPolyline.length();
		int lat = 0, lng = 0;

		while (index < len) {
			int b, shift = 0, result = 0;
			do {
				b = encodedPolyline.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;

			shift = 0;
			result = 0;
			do {
				b = encodedPolyline.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;

			LatLng p = new LatLng((((double) lat / 1E5)),
					(((double) lng / 1E5)));
			poly.add(p);
		}

		return poly;		
	}
}
